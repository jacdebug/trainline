import { css } from "react-emotion";

export const paddingFive = css`
  padding: 5px;
`;

export const flexContainer = css`
  display: flex;
  flex-wrap: wrap;
  ${paddingFive};
`;

// 100 percent width on mobile devices
export const flexItem = css`
  min-width: 350px;
  flex: 0 0 100%;
  @media (min-width: 768px) {
    flex: 1;
  }
`;

// move top on mobile devices
export const serviceDetailFlexItem = css`
  ${flexItem} order: -1;
  @media (min-width: 768px) {
    order: 0;
  }
`;
