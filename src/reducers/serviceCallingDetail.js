import { GET_CALLING_PATTERN } from "../actions";
import { formatTime, getStationName, getOperatorName } from "../util";

const initialState = {};
const ON_TIME = "On time";

// create real time status for stop
const processRealTimeInfo = ({
  scheduledTime,
  realTime,
  realTimeFlag,
  hasDeparted
}) =>
  scheduledTime === realTime
    ? ON_TIME
    : realTimeFlag === "Estimate" || realTimeFlag === "Actual"
      ? `${hasDeparted ? "Dept" : "Exp"}. ${formatTime(realTime)}`
      : "";

// normalize stops array to data what we need
const stopsTrasform = stops => {
  let departedIndex = -1;

  // transform stops
  const result = stops.map((stop, index) => {
    let scheduledArrival = "";
    let realTimeInfo = "";

    // for unhandled case where object keys are not available
    try {
      // show real time info if available
      if (stop.departure.realTime && !stop.departure.realTime.cancelled) {
        if (stop.departure.realTime.realTimeServiceInfo.hasDeparted) {
          departedIndex = index;
        }
        realTimeInfo = processRealTimeInfo({
          scheduledTime: stop.departure.scheduled.scheduledTime,
          realTime: stop.departure.realTime.realTimeServiceInfo.realTime,
          realTimeFlag:
            stop.departure.realTime.realTimeServiceInfo.realTimeFlag,
          hasDeparted: stop.departure.realTime.realTimeServiceInfo.hasDeparted
        });
      }

      // arrival not available on origin station
      if (!stop.arrival || stop.arrival.notApplicable) {
        scheduledArrival = formatTime(stop.departure.scheduled.scheduledTime);
      } else {
        // get scheduled time fallback real time
        let arrivalTime = stop.arrival.scheduled
          ? stop.arrival.scheduled.scheduledTime
          : stop.arrival.realTime
            ? stop.arrival.realTime.realTimeServiceInfo.realTime
            : "";
        scheduledArrival = arrivalTime && formatTime(arrivalTime);
      }
    } catch (e) {
      // report unhanded cases to sever
      console.error("Trainline api error", e);
    }

    return {
      realTimeInfo,
      location: getStationName(stop.location.crs),
      scheduledArrival: scheduledArrival,
      realTimeInfoInRed: realTimeInfo && realTimeInfo !== ON_TIME
    };
  });

  // set departed if not last stop and if departed
  if (departedIndex < result.length - 1 && departedIndex > -1)
    result[departedIndex].departed = true;

  return result;
};

// service calling detail reducer
const serviceCallingDetail = (state = initialState, action) => {
  switch (action.type) {
    case GET_CALLING_PATTERN.SUCCESS:
      return {
        origin: getStationName(action.service.serviceOrigins[0]),
        destination: getStationName(action.service.serviceDestinations[0]),
        operator: getOperatorName(action.service.serviceOperator),
        stops: stopsTrasform(action.service.stops)
      };
    default:
      return state;
  }
};

export default serviceCallingDetail;
