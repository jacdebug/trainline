import { GET_LATEST_DEPARTURE } from "../actions";
import { HOSTNAME_LENGTH } from "../util";
import { formatTime, getStationName, getOperatorName } from "../util";

const initialState = [];
const DELAYED = "Delayed";

// process real time status info
export const processRealTimeStatus = (info, scheduledTime) => {
  if (info.realTimeFlag === "Estimate") {
    return info.realTime !== scheduledTime
      ? `Exp. ${formatTime(info.realTime)}`
      : "On time";
  } else if (info.realTimeFlag === DELAYED) {
    return DELAYED;
  } else {
    return "";
  }
};

// process real time information of a service
export const processRealTimeInfo = service => {
  // fallback values for no real time info
  let realTimeInfo = "";
  let isCancelled = false;
  let platform = service.scheduledInfo.scheduledPlatform;

  // real time info available
  if (service.realTimeUpdatesInfo) {
    if (service.realTimeUpdatesInfo.realTimeServiceInfo) {
      // save reference to avoid lookup chain
      let realTimeServiceInfo = service.realTimeUpdatesInfo.realTimeServiceInfo;

      // update platform if real time platform details available
      if (realTimeServiceInfo.realTimePlatform) {
        platform = realTimeServiceInfo.realTimePlatform;
      }
      // process real time info
      realTimeInfo = processRealTimeStatus(
        realTimeServiceInfo,
        service.scheduledInfo.scheduledTime
      );
    } else if (
      service.realTimeUpdatesInfo.cancelled &&
      service.realTimeUpdatesInfo.cancelled.isCancelled
    ) {
      // check if service cancelled
      isCancelled = true;
      realTimeInfo = "Cancelled";
    }
  }
  return { platform, isCancelled, realTimeInfo };
};

// map over each service and create normalized data to store
const serviceTrasform = service => ({
  scheduledTime: formatTime(service.scheduledInfo.scheduledTime),
  destination: getStationName(service.destinationList[0].crs),
  operator: getOperatorName(service.serviceOperator),
  callingPatternUrl: service.callingPatternUrl.slice(HOSTNAME_LENGTH),
  ...processRealTimeInfo(service)
});

// service reducer
export const service = (state = initialState, action) => {
  switch (action.type) {
    case GET_LATEST_DEPARTURE.SUCCESS:
      return action.services
        .filter(service => service.transportMode === "TRAIN")
        .map(serviceTrasform);
    default:
      return state;
  }
};
