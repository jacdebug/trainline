import { processRealTimeStatus, processRealTimeInfo } from "./service";

// test processRealTimeStatus
const testCaseProcessRealTimeStatus = [
  {
    info: {
      realTimeFlag: "Estimate",
      realTime: "2018-03-22T19:25:00+00:00"
    },
    scheduledTime: "2018-03-22T19:25:00+00:00",
    expected: "On time"
  },
  {
    info: {
      realTimeFlag: "Estimate",
      realTime: "2018-03-22T19:25:00+00:00"
    },
    scheduledTime: "2018-03-22T19:20:00+00:00",
    expected: "Exp. 19:25"
  },
  {
    info: {
      realTimeFlag: "Delayed",
      realTime: "2018-03-22T19:25:00+00:00"
    },
    scheduledTime: "2018-03-22T19:20:00+00:00",
    expected: "Delayed"
  },
  {
    info: {
      realTimeFlag: "",
      realTime: ""
    },
    scheduledTime: "",
    expected: ""
  }
];

test("processRealTimeStatus should return status", () => {
  testCaseProcessRealTimeStatus.forEach(test => {
    expect(processRealTimeStatus(test.info, test.scheduledTime)).toBe(
      test.expected
    );
  });
});

// test testCaseProcessRealTimeInfo
const testCaseProcessRealTimeInfo = [
  {
    service: {
      scheduledInfo: {
        scheduledTime: "2018-03-22T20:11:00+00:00",
        scheduledPlatform: "8"
      }
    },
    expected: { isCancelled: false, platform: "8", realTimeInfo: "" }
  },
  {
    service: {
      scheduledInfo: {
        scheduledTime: "2018-03-22T20:11:00+00:00",
        scheduledPlatform: "8"
      },
      realTimeUpdatesInfo: {
        cancelled: { isCancelled: true }
      }
    },
    expected: { isCancelled: true, platform: "8", realTimeInfo: "Cancelled" }
  },
  {
    service: {
      scheduledInfo: {
        scheduledTime: "2018-03-22T20:11:00+00:00",
        scheduledPlatform: "8"
      },
      realTimeUpdatesInfo: {
        realTimeServiceInfo: {
          realTime: "2018-03-22T20:19:00+00:00",
          realTimePlatform: "8",
          realTimeFlag: "Estimate"
        }
      }
    },
    expected: { isCancelled: false, platform: "8", realTimeInfo: "Exp. 20:19" }
  }
];

test("processRealTimeInfo should return platform, cancelled, realtime", () => {
  testCaseProcessRealTimeInfo.forEach(test => {
    expect(processRealTimeInfo(test.service)).toEqual(test.expected);
  });
});
