import { combineReducers } from "redux";
import { service } from "./service";
import serviceCallingDetail from "./serviceCallingDetail";

export const rootReducer = combineReducers({
  service,
  serviceCallingDetail
});
