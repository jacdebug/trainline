import tinytime from "tinytime";
import _operators from "../data/operators.json";
import _stations from "../data/stations.json";

// create tinytime template for displaying train time
const trainTimeTmpl = tinytime("{H}:{mm}", { padHours: true });

/**
 * Format time in train time format
 * @param {string} date string
 * @return {string} date formatted
 */
export const formatTime = timeString =>
  trainTimeTmpl.render(new Date(timeString));

// curried function to get key if key exists
export const keyValueLookup = dic => key => (dic[key] ? dic[key] : dic);

// partial apply curried function to create lookups needed
export const getStationName = keyValueLookup(_stations);
export const getOperatorName = keyValueLookup(_operators);

// to fix api for working with proxy
export const HOSTNAME_LENGTH = 'https://realtime.thetrainline.com'.length;
