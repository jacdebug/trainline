import "rxjs";
import { combineEpics } from "redux-observable";
import { api } from "../services";
import * as actions from "../actions";

const getLatestDeparture = action$ =>
  action$
    .ofType(actions.GET_LATEST_DEPARTURE.REQUEST)
    .switchMap(q =>
      api.getLatestDeparture().map(actions.getLatestDepartureSuccess)
    );

const getCallingPattern = action$ =>
  action$
    .ofType(actions.GET_CALLING_PATTERN.REQUEST)
    .switchMap(q =>
      api.getCallingPattern(q.url).map(actions.getCallingPatternSuccess)
    );

export const rootEpic = combineEpics(getLatestDeparture, getCallingPattern);
