import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { createEpicMiddleware } from "redux-observable";
import { BrowserRouter as Router, Route } from "react-router-dom";
import * as actions from "./actions";
import { rootEpic } from "./epics";
import { rootReducer } from "./reducers";
import {
  paddingFive,
  flexContainer,
  flexItem,
  serviceDetailFlexItem
} from "./AppStyles";
import ServiceListContainer from "./components/ServiceList/ServiceListContainer";
import ServiceDetailContainer from "./components/ServiceDetail/ServiceDetailContainer";

const epicMiddleware = createEpicMiddleware(rootEpic);
// redux state store, built with the Epic middleware.
const store = createStore(rootReducer, applyMiddleware(epicMiddleware));
store.dispatch(actions.getLatestDeparture());

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Router>
            <div className={flexContainer}>
              <div className={flexItem}>
                <div className={paddingFive}>
                  <Route path="/" component={ServiceListContainer} />
                </div>
              </div>
              <div className={serviceDetailFlexItem}>
                <div className={paddingFive}>
                  <Route
                    path="/details/:url"
                    component={ServiceDetailContainer}
                  />
                </div>
              </div>
            </div>
          </Router>
        </div>
      </Provider>
    );
  }
}

export default App;
