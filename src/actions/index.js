// constants for creating request actions
const REQUEST = "REQUEST";
const SUCCESS = "SUCCESS";
const FAILURE = "FAILURE";

// action object creator
export const action = (type, payload = {}) => ({ type, ...payload });

// create server request actions
export const createRequestTypes = base =>
  [REQUEST, SUCCESS, FAILURE].reduce((acc, type) => {
    acc[type] = `${base}_${type}`;
    return acc;
  }, {});

// latest departure actions and action creators
export const GET_LATEST_DEPARTURE = createRequestTypes("GET_LATEST_DEPARTURE");
export const getLatestDeparture = () => action(GET_LATEST_DEPARTURE[REQUEST]);
export const getLatestDepartureSuccess = services =>
  action(GET_LATEST_DEPARTURE[SUCCESS], services);

// calling pattern actions and action creators
export const GET_CALLING_PATTERN = createRequestTypes("GET_CALLING_PATTERN ");
export const getCallingPattern = url =>
  action(GET_CALLING_PATTERN[REQUEST], { url });
export const getCallingPatternSuccess = serviceCallingPattern =>
  action(GET_CALLING_PATTERN[SUCCESS], serviceCallingPattern);
