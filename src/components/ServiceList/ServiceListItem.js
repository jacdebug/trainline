import React from "react";
import { Link } from "react-router-dom";
import * as styles from "./ServiceListStyles";
import styled from "react-emotion";
 
const RealTimeInfo = styled("div")`
  ${styles.realTime} ${props => ({
    color: props.red ? "red" : "#197328"
  })};
`;

const SeriveItemLink = styled(Link)`
  ${styles.wrapper} ${props =>
    props.style.isCancelled &&
    `
    pointer-events: none;
  `};
`;

export const ServiceListItem = ({ service }) => (
  <SeriveItemLink
    style={{ isCancelled: service.isCancelled }}
    to={
      service.isCancelled ? "#" : `/details/${btoa(service.callingPatternUrl)}`
    }
  >
    <div className={styles.service}>
      <span className={styles.time}>{service.scheduledTime}</span>
      <span className={styles.destination}>{service.destination}</span>
      <span className={styles.platform}>Plat. {service.platform}</span>
    </div>
    <div className={styles.subInfo}>
      <span className={styles.operator}>{service.operator}</span>
      <RealTimeInfo
        red={service.realTimeInfo && service.realTimeInfo !== "On time"}
      >
        {service.realTimeInfo}
      </RealTimeInfo>
    </div>
  </SeriveItemLink>
);

export default ServiceListItem;
