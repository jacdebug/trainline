import { css } from "react-emotion";

export const wrapper = css`
  background: #fff;
  border-bottom: 1px solid #eee;
  text-decoration: none;
  display: block;
  span {
    display: inline-block;
  }
`;

export const service = css`
  display: flex;
  font-size: 16px;
  padding: 10px 10px 5px 10px;
  align-items: baseline;
}
`;

export const time = css`
  width: 50px;
  text-align: left;
  font-weight: 700;
  letter-spacing: -0.05px;
`;

export const destination = css`
  flex: 1;
  text-align: left;
  padding-left: 5px;
`;

export const platform = css`
  text-align: right;
  padding-left: 5px;
`;

export const subInfo = css`
  ${service} font-size: 14px;
  padding: 0 10px 10px 60px;
`;

export const operator = css`
  ${destination};
  color: #8c9190;
`;

export const realTime = css`
  ${platform};
`;
