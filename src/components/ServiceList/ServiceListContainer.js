import React from "react";
import { connect } from "react-redux";
import ServiceListItem from "./ServiceListItem";

const ServiceList = ({ services, ...props }) =>
  services.map((service, index) => (
    <ServiceListItem key={index} service={service} {...props} />
  ));

const mapStateToProps = state => ({
  services: state.service
});

export default connect(mapStateToProps)(ServiceList);
