import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import ServiceListItem from "./ServiceListItem";

it("<ServiceListItem /> renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter>
      <ServiceListItem
        service={{
          isCancelled: false,
          callingPatternUrl: "",
          scheduledTime: "00:00",
          destination: "Euston",
          platform: 5,
          operator: "London midland",
          realTimeInfo: "On time"
        }}
      />
    </MemoryRouter>,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
