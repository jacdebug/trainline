import React from "react";
import { css } from "react-emotion";
import ServiceDetailStop  from "./ServiceDetailStop";
import trainIcon from "./train_icon.png";
 
const wrapper = css`
  background: #fff;
`;

const serviceHead = css`
  background: url(${trainIcon}) no-repeat 10px 10px;
  background-size: 30px;
  padding: 10px 10px 10px 70px;
  text-align: left;
`;

const stationName = css`
  font-weight: 700;
`;

const operatorTitle = css`
  color: #8c9190;
  font-size: 13px;
`;

const toText = css`
  color: #ccc;
  font-size: 14px;
`;

const ServiceDetail = ({ serviceDetails, ...props }) => (
  <div className={wrapper}>
    <div className={serviceHead}>
      <div className={stationName}>{serviceDetails.origin}</div>
      <div>
        <span className={toText}>to</span>{" "}
        <span className={stationName}>{serviceDetails.destination}</span>
      </div>
      <div className={operatorTitle}>{serviceDetails.operator}</div>
    </div>
    <div>
      {serviceDetails.stops &&
        serviceDetails.stops.map((stop, index, stops) => (
          <ServiceDetailStop
            key={index}
            index={index}
            stop={stop}
            totalStops={stops.length}
          />
        ))}
    </div>{" "}
  </div>
);

export default ServiceDetail;
