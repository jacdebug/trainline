import React from "react";
import styled, { css } from "react-emotion";

// needs css refactor
const wrapper = css`
  display: flex;
`;

const time = css`
  width: 50px;
  text-align: left;
  font-weight: 700;
  letter-spacing: -0.05px;
  padding: 0 10px;
`;

const ServiceDetailStopInfo = styled("div")`
  padding: 0px 10px 10px 20px;
  position: relative;
  text-align: left;

  ${props => ({
    "border-left": `1px solid ${props.isLast ? "#fff" : "#999"}`
  })} &:after {
    content: " ";
    display: block;
    width: 11px;
    height: 11px;
    border-radius: 50%;
    border: 1px solid #999;
    position: absolute;
    top: 0;
    left: -7px;
    ${props => ({
      background: props.isStart || props.isLast ? "#000" : "#fff"
    })};
  }

  ${props =>
    props.isDeparted
      ? `

  &:before {
    content: " ";
    display: block;
    width: 15px;
    height: 15px;
    border-radius: 50%;
    border: 1px solid #999;
    position: absolute;
    top: 18px;
    left: -9px;
    background: #48d5b5;
  }`
      : ""};
`;

const realTimeInfo = css`
  color: #8c9190;
  font-size: 12px;
`;

const ServiceDetailStop = ({ stop, index, totalStops, departed }) => (
  <div className={wrapper}>
    <div className={time}>{stop.scheduledArrival}</div>
    <ServiceDetailStopInfo
      isDeparted={stop.departed}
      isStart={index === 0}
      isLast={totalStops === index + 1}
    >
      <div>{stop.location}</div>
      <div className={realTimeInfo}>{stop.realTimeInfo}</div>
    </ServiceDetailStopInfo>
  </div>
);

export default ServiceDetailStop;
