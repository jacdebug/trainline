import React, { Component } from "react";
import { connect } from "react-redux";
import { getCallingPattern } from "../../actions";
import ServiceDetail from "./ServiceDetail";

const mapStateToProps = state => ({
  serviceDetails: state.serviceCallingDetail
});

const mapDispatchToProps = dispatch => ({
  onServiceClick(url) {
    dispatch(getCallingPattern(url));
  }
});

class ServiceDetailContainer extends Component {
  componentWillMount() {
    this.props.onServiceClick(atob(this.props.match.params.url));
  }
  componentWillReceiveProps({ onServiceClick, match }) {
    if (match.params.url !== this.props.match.params.url) {
      onServiceClick(atob(match.params.url));
    }
  }
  render() {
    return <ServiceDetail {...this.props} />;
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ServiceDetailContainer
);
