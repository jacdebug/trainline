import React from "react";
import ReactDOM from "react-dom";
import ServiceDetailStop from "./ServiceDetailStop";

it("<ServiceDetailStop /> renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <ServiceDetailStop
      key={0}
      index={0}
      stop={{
        scheduledArrival: "00:00",
        departed: false,
        location: "Waterloo",
        realTimeInfo: "Exp: 00:00"
      }}
      totalStops={5}
    />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});
