import React from "react";
import ReactDOM from "react-dom";
import ServiceDetail from "./ServiceDetail";

it("<ServiceDetail /> renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <ServiceDetail
      serviceDetails={{
        origin: "Waterloo",
        destination: "Basingstoke",
        operator: "virgin",
        stops: []
      }}
    />,
    div
  );

  ReactDOM.unmountComponentAtNode(div);
});
