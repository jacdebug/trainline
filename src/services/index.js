import { Observable } from "rxjs/Observable";

// curried function for GET from sever
const get = url => () => Observable.ajax(url).map(e => e.response);

// call the url and pass Observable
const getLatestDeparture = get("/departures/wfj"); // wat wfj bko

// url is dynamic and read from router for calling pattern
const getCallingPattern = url => get(url)();

export const api = {
  getLatestDeparture,
  getCallingPattern
};
